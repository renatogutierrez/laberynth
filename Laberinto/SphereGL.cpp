//
//  SphereGL.cpp
//  Paint3D
//
//  Created by Alejandro Carrillo on 3/26/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#include "SphereGL.hpp"

SphereGL::SphereGL() : ObjectGL(){
    this->size = 0.5f;
}
SphereGL::~SphereGL(){
}

void SphereGL::render(){
    glPushMatrix();
    
    glColor3f(this->color[0], this->color[1], this->color[2]);
    
    GLfloat *ptrM = glm::value_ptr(this->m);
    glMultMatrixf(ptrM);
    
    if(this->style == GL_LINE){
        glutWireSphere(this->size, 25, 25);
    }else{
        glutSolidSphere(this->size, 200, 200);
    }
    
    
    glPopMatrix();
}

GLfloat SphereGL::getRadius(){
    return this->size;
}
