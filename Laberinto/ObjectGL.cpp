//
//  ObjectGL.cpp
//  Paint3D
//
//  Created by Renato Uriel Gutierrez Salas on 18/03/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#include "ObjectGL.hpp"

ObjectGL::ObjectGL(){
    this->setColor(1.f, 1.f, 1.f);
    
    this->centroid = new CoordinateGL(0.f, 0.f, 0.f);
}

void ObjectGL::setColor(GLfloat r, GLfloat g, GLfloat b){
    this->color[0] = r;
    this->color[1] = g;
    this->color[2] = b;
    this->color[3] = 1.f;
}

void ObjectGL::setFilled(GLboolean filled){
    this->style = (filled) ? GL_FILL : GL_LINE;
}

void ObjectGL::scale(GLfloat sx, GLfloat sy, GLfloat sz){
    this->m = glm::scale(this->m, glm::vec3(sx, sy, sz));
}

void ObjectGL::translate(GLfloat tx, GLfloat ty, GLfloat tz){
    this->m = glm::translate(this->m, glm::vec3(tx, ty, tz));
    
    this->centroid->addX(tx);
    this->centroid->addY(ty);
    this->centroid->addZ(tz);
}

void ObjectGL::rotate(GLfloat angle, GLfloat rx, GLfloat ry, GLfloat rz){
    /*
     a b c d
     e f g h
     i j k l
     0 0 0 1
     
     x = d
     y = h
     z = l
     
     Move to origin
     translate(-d, -h, -l)
     
     Rotate
     
     Return to original position
     translate(d, h, l)
     */
    
    //this->m = glm::translate(this->m, glm::vec3(this->m[0][3], this->m[1][3], this->m[2][3]));
    
    this->m = glm::rotate(this->m, glm::radians(angle), glm::vec3(rx, ry, rz));
    
    //this->m = glm::translate(this->m, glm::vec3(this->m[0][3], this->m[1][3], this->m[2][3]));
}

CoordinateGL* ObjectGL::getCentroid(){
    return this->centroid;
}
