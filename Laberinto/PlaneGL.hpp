//
//  PlaneGL.hpp
//  Paint3D
//
//  Created by Renato Uriel Gutierrez Salas on 18/03/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#ifndef PlaneGL_hpp
#define PlaneGL_hpp

#include "ObjectGL.hpp"

class PlaneGL : public ObjectGL{
private:
    GLfloat size;
public:
    PlaneGL();
    ~PlaneGL();
    void render();
    GLfloat getSize();
};

#endif /* PlaneGL_hpp */
