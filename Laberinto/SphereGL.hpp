//
//  SphereGL.hpp
//  Paint3D
//
//  Created by Alejandro Carrillo on 3/26/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#ifndef SphereGL_hpp
#define SphereGL_hpp

#include "ObjectGL.hpp"
#include "CoordinateGL.hpp"

class SphereGL : public ObjectGL{
private:
    GLfloat size;
public:
    SphereGL();
    ~SphereGL();
    void render();
    GLfloat getRadius();
};

#endif /* SphereGL_hpp */
