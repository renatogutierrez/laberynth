//
//  BoxGL.cpp
//  Paint3D
//
//  Created by Renato Uriel Gutierrez Salas on 18/03/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#include "BoxGL.hpp"

BoxGL::BoxGL() : ObjectGL(){
    this->size = 1.f;
    this->size = this->size / 2;
}
BoxGL::~BoxGL(){
}

void BoxGL::render(){
    glPushMatrix();
    
    GLfloat *ptrM = glm::value_ptr(this->m);
    glMultMatrixf(ptrM);
    
    GLfloat faceSize;
    
    PlaneGL *face1 = new PlaneGL();
    faceSize = face1->getSize();
    
    face1->setColor(this->color[0], this->color[1], this->color[2]);
    face1->style = this->style;
    face1->translate(0.f, 0.f, -faceSize / 2);
    face1->render();
    
    PlaneGL *face2 = new PlaneGL();
    face2->setColor(this->color[0], this->color[1], this->color[2]);
    face2->style = this->style;
    face2->translate(0.f, 0.f, faceSize / 2);
    face2->render();
    
    PlaneGL *face3 = new PlaneGL();
    face3->setColor(this->color[0], this->color[1], this->color[2]);
    face3->style = this->style;
    face3->rotate(90, 1.f, 0.f, 0.f);
    face3->translate(0.f, 0.f, faceSize / 2);
    face3->render();
    
    PlaneGL *face4 = new PlaneGL();
    face4->setColor(this->color[0], this->color[1], this->color[2]);
    face4->style = this->style;
    face4->rotate(90, 1.f, 0.f, 0.f);
    face4->translate(0.f, 0.f, -faceSize / 2);
    face4->render();
    
    PlaneGL *face5 = new PlaneGL();
    face5->setColor(this->color[0], this->color[1], this->color[2]);
    face5->style = this->style;
    face5->rotate(90, 0.f, 1.f, 0.f);
    face5->translate(0.f, 0.f, faceSize / 2);
    face5->render();
    
    PlaneGL *face6 = new PlaneGL();
    face6->setColor(this->color[0], this->color[1], this->color[2]);
    face6->style = this->style;
    face6->rotate(90, 0.f, 1.f, 0.f);
    face6->translate(0.f, 0.f, -faceSize / 2);
    face6->render();
    
    glPopMatrix();
}

GLfloat BoxGL::getRadius(){
    return this->size;
}
