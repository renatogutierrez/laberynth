//
//  PlaneGL.cpp
//  Paint3D
//
//  Created by Renato Uriel Gutierrez Salas on 18/03/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#include "PlaneGL.hpp"

PlaneGL::PlaneGL() : ObjectGL(){
    this->size = 1.f;
}
PlaneGL::~PlaneGL(){
}

void PlaneGL::render(){
    glPushMatrix();
    
    glPolygonMode(GL_FRONT, this->style);
    glPolygonMode(GL_BACK, this->style);
    glColor3f(this->color[0], this->color[1], this->color[2]);
    
    GLfloat *ptrM = glm::value_ptr(this->m);
    glMultMatrixf(ptrM);
    
    glBegin(GL_TRIANGLES);
    glVertex3f(-this->size / 2, -this->size / 2, 0.f);
    glVertex3f(this->size / 2, -this->size / 2, 0.f);
    glVertex3f(this->size / 2, this->size / 2, 0.f);
    glEnd();
    
    glBegin(GL_TRIANGLES);
    glVertex3f(-this->size / 2, -this->size / 2, 0.f);
    glVertex3f(this->size / 2, this->size / 2, 0.f);
    glVertex3f(-this->size / 2, this->size / 2, 0.f);
    glEnd();
    
    glPopMatrix();
}

GLfloat PlaneGL::getSize(){
    return this->size;
}
