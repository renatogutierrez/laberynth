//
//  BoxGL.hpp
//  Paint3D
//
//  Created by Renato Uriel Gutierrez Salas on 18/03/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#ifndef BoxGL_hpp
#define BoxGL_hpp

#include "PlaneGL.hpp"
#include "ObjectGL.hpp"
#include "CoordinateGL.hpp"

class BoxGL : public ObjectGL{
private:
    GLfloat size;
public:
    BoxGL();
    ~BoxGL();
    void render();
    GLfloat getRadius();
};

#endif /* BoxGL_hpp */
