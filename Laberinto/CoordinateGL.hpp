//
//  CoordinateGL.hpp
//  Laberinto
//
//  Created by Renato Uriel Gutierrez Salas on 20/04/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#ifndef CoordinateGL_hpp
#define CoordinateGL_hpp

#include <OpenGL/OpenGL.h>

class CoordinateGL {
private:
    GLfloat x;
    GLfloat y;
    GLfloat z;
public:
    CoordinateGL(GLfloat x, GLfloat y, GLfloat z);
    ~CoordinateGL();
    GLfloat getX();
    GLfloat getY();
    GLfloat getZ();
    void addX(GLfloat x);
    void addY(GLfloat y);
    void addZ(GLfloat z);
};

#endif /* CoordinateGL_hpp */
