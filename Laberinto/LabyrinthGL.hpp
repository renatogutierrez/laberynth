//
//  LabyrinthGL.hpp
//  Laberinto
//
//  Created by Alejandro Carrillo on 4/20/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#ifndef LabyrinthGL_hpp
#define LabyrinthGL_hpp

#include <stdio.h>
#include "ObjectGL.hpp"
#include "BoxGL.hpp"

class LabyrinthGL : public ObjectGL{

public:
    LabyrinthGL();
    ~LabyrinthGL();
    void render();
    
};


#endif 
