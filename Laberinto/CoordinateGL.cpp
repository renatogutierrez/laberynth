//
//  CoordinateGL.cpp
//  Laberinto
//
//  Created by Renato Uriel Gutierrez Salas on 20/04/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#include "CoordinateGL.hpp"

CoordinateGL::CoordinateGL(GLfloat x, GLfloat y, GLfloat z){
    this->x = x;
    this->y = y;
    this->z = z;
    
}
CoordinateGL::~CoordinateGL(){
}
GLfloat CoordinateGL::getX(){
    return this->x;
}
GLfloat CoordinateGL::getY(){
    return this->y;
}
GLfloat CoordinateGL::getZ(){
    return this->z;
}

void CoordinateGL::addX(GLfloat x){
    this->x += x;
}
void CoordinateGL::addY(GLfloat y){
    this->y += y;
}
void CoordinateGL::addZ(GLfloat z){
    this->z += z;
}
