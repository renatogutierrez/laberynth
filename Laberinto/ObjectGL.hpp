//
//  ObjectGL.hpp
//  Paint3D
//
//  Created by Renato Uriel Gutierrez Salas on 18/03/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#ifndef ObjectGL_hpp
#define ObjectGL_hpp

#include <OpenGL/OpenGL.h>
#include <GLUT/GLUT.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include "glm/gtc/type_ptr.hpp"
#include "CoordinateGL.hpp"

class ObjectGL{
public:
    glm::mat4 m;
    GLfloat color[4];
    GLint style;
    
    CoordinateGL* centroid;
public:
    ObjectGL();
    void setColor(GLfloat r, GLfloat g, GLfloat b);
    void setFilled(GLboolean filled);
    void scale(GLfloat sx, GLfloat sy, GLfloat sz);
    void translate(GLfloat tx, GLfloat ty, GLfloat tz);
    void rotate(GLfloat angle, GLfloat rx, GLfloat ry, GLfloat rz);
    
    CoordinateGL* getCentroid();
    virtual void render() = 0;
};

#endif /* ObjectGL_hpp */
