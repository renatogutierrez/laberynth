//
//  main.cpp
//  Laberinto
//
//  Created by Renato Uriel Gutierrez Salas on 20/04/17.
//  Copyright © 2017 Renato Uriel Gutierrez Salas. All rights reserved.
//

#include <iostream>
#include <OpenGL/OpenGL.h>
#include <GLUT/GLUT.h>

#include "PlaneGL.hpp"
#include "SphereGL.hpp"
#include "BoxGL.hpp"
#include "LabyrinthGL.hpp"

using namespace std;

// TamaÒo de la ventana
GLint width = 800;
GLint height = 600;
//Idle
GLfloat rot = 0.f;
GLfloat movX = 1.f;
GLfloat movZ = 1.f;
GLfloat movY = 1.f;
GLfloat speed = .5f;
GLfloat deltaRot = 0.5f;
// Par·metros de la c·mara virtual
GLint mouseButton = 0;
GLboolean mouseRotate = false;
GLboolean mouseZoom = false;
GLboolean mousePan = false;
GLint xClick = 0;
GLint yClick = 0;
GLint xAux = 0;
GLint yAux = 0;
GLfloat rotX = 0.f;
GLfloat rotY = 0.f;
GLfloat panX = 0.f;
GLfloat panY = 0.f;
GLfloat zoomZ = -10.f;

// FunciÛn que inicializa el Contexto OpenGL y la geometrÌa de la escena
void init()
{
    glClearColor(0.f, 0.f, 0.f, 0.f);
    glColor3f(1.f, 1.f, 1.f);
}

// FunciÛn que se invoca cada vez que se redimensiona la ventana
void resize(GLint w, GLint h)
{
    if (h == 0)
    {
        h = 1;
    }
    width = w;
    height = h;
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    gluPerspective(25.f, (float)width / height, 0.1f, 1000.f);		// Transf. de ProyecciÛn en Perspectiva
    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, width, height);								// Transf. de Viewport (mapeo al ·rea de dibujo)
}

// GeometrÌa sistema coordenado
void geomCoordSys(GLfloat size)
{
    // Eje X (Rojo)
    glColor3f(1., 0., 0.);
    glBegin(GL_LINES);
    glVertex3f(0., 0., 0.);
    glVertex3f(size, 0., 0.);
    glEnd();
    
    // Eje Y (Verde))
    glColor3f(0., 1., 0.);
    glBegin(GL_LINES);
    glVertex3f(0., 0., 0.);
    glVertex3f(0., size, 0.);
    glEnd();
    
    // Eje Z (Azul)
    glColor3f(0., 0., 1.);
    glBegin(GL_LINES);
    glVertex3f(0., 0., 0.);
    glVertex3f(0., 0., size);
    glEnd();
    glColor3f(1., 1., 1.);
}

// Collision Detection
bool isColliding(SphereGL* A, BoxGL* B){
    
    GLfloat dx = A->getCentroid()->getX() - B->getCentroid()->getX();
    GLfloat dy = A->getCentroid()->getY() - B->getCentroid()->getY();
    GLfloat dz = A->getCentroid()->getZ() - B->getCentroid()->getZ();
    
    GLfloat distance = sqrt(dx*dx + dy*dy + dz*dz);
    
    if (distance <= (A->getRadius() + B->getRadius()) ) {
        return true;
    }
    
    return false;
}

// FunciÛn que se invoca cada vez que se dibuja
void render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	//Se limpian los buffers con el color activo definido por glClearColor
    
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
    
    // TransformaciÛn de vista
    glTranslatef(panX, panY, zoomZ);
    glRotatef(rotY, 1.0, 0.0, 0.0);
    glRotatef(rotX, 0.0, 1.0, 0.0);
    
    // Escena
    LabyrinthGL* lab = new LabyrinthGL();
    lab->render();
    
    SphereGL* ball = new SphereGL();
    ball->setFilled(false);
    
    cout << "Ball centroid: x = " << ball->getCentroid()->getX() << endl;
    cout << "               y = " << ball->getCentroid()->getY() << endl;
    cout << "               z = " << ball->getCentroid()->getZ() << endl;
    cout << "               radius = " << ball->getRadius() << endl;
    
    ball->render();
    
    BoxGL* box = new BoxGL();
    box->setFilled(false);
    box->translate(0.f, 0.f, 1.01f);
    
    cout << "Box centroid: x = " << box->getCentroid()->getX() << endl;
    cout << "              y = " << box->getCentroid()->getY() << endl;
    cout << "              z = " << box->getCentroid()->getZ() << endl;
    cout << "              radius = " << box->getRadius() << endl;
    
    box->render();
    
    cout << "Ball & Box are colliding?: " << isColliding(ball, box) << endl;
    
    geomCoordSys(2.f);
    
    glutSwapBuffers();			// Se intercambian buffers
}



void mouse(int button, int state, int x, int y)
{
    mouseButton = button;
    mouseRotate = false;
    mouseZoom = false;
    mousePan = false;
    xClick = x;
    yClick = y;
    if (button == GLUT_LEFT_BUTTON)
    {
        mouseRotate = true;
        xAux = rotX;
        yAux = rotY;
    }
    else if (button == GLUT_RIGHT_BUTTON)
    {
        mouseZoom = true;
        xAux = zoomZ;
    }
    else if (button == GLUT_MIDDLE_BUTTON)
    {
        mousePan = true;
        xAux = panX;
        yAux = panY;
    }
}

void mouseMotion(int x, int y)
{
    if (mouseRotate == true)
    {
        if (mouseButton == GLUT_LEFT_BUTTON)
        {
            if ((x - xClick + xAux) > 360.0)
            {
                xAux = xAux - 360.0;
            }
            if ((x - xClick + xAux) < 0.0)
            {
                xAux = xAux + 360.0;
            }
            if ((y - yClick + yAux) > 360.0)
            {
                yAux = yAux - 360.0;
            }
            if ((y - yClick + yAux) < 0.0)
            {
                yAux = yAux + 360.0;
            }
            rotX = x - xClick + xAux;
            rotY = y - yClick + yAux;
        }
    }
    else if (mouseZoom == true)
    {
        if (mouseButton == GLUT_RIGHT_BUTTON)
        {
            zoomZ = ((x - xClick) / 10.0) + xAux;
        }
    }
    else if (mousePan == true)
    {
        if (mouseButton == GLUT_MIDDLE_BUTTON)
        {
            panX = ((x - xClick) / 63.0) + xAux;
            panY = ((y - yClick) / (-63.0)) + yAux;
        }
    }
    glutPostRedisplay();
}
void idle(){
    if(rotX < 180 && rotX >0){
        movX += rotX*0.0001f;
    } else {
        if(rotX > 180){
            movX -= (rotX-180)*0.0001f;
        } else {
            
        }
    }
    
    
    if(rotY < 180 && rotY >0){
        movZ += rotY*0.0001f;
    } else {
        if(rotY > 180){
            movZ -= (rotY-180)*0.0001f;
        } else {
            
        }
    }
    //rot += deltaRot;
    glutPostRedisplay();
    //cout << rotX << endl;
    cout << rotY << endl;
    
}
int main(GLint argc, GLchar **argv)
{
    // 1. Se crea una ventana y un contexto OpenGL usando GLUT
    glutInit(&argc, argv);
    glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA | GLUT_DEPTH);
    glutInitWindowSize(width, height);
    glutCreateWindow("Hello World OpenGL with Interactive Camera");
    
    // 1.2 Se definen las funciones callback para el manejo de eventos
    glutReshapeFunc(resize);
    glutDisplayFunc(render);
    glutMouseFunc(mouse);
    glutMotionFunc(mouseMotion);
    
    // 2. Se direcciona a las funciones correctas del API de OpenGL
    
    // 3. Se inicializa el contexto de OpenGL y la geometrÌa de la escena
    init();
    
    // 4. Se inicia el ciclo de escucha de eventos
    glutMainLoop();
    return 0;
}
